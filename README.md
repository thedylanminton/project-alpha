# Project Management Demo App

Welcome to the Project Management Demo App! This application provides a simplified demonstration of project management functionalities, helping users organize and track tasks, collaborate with team members, and monitor project progress.

## Features

1. **Task Management:** Create, edit, and delete tasks to keep track of your project's to-do list.

2. **Project Overview:** View a comprehensive overview of all tasks within a project, including their status and assignees.

5. **Due Dates and Prioritization:** Set due dates for tasks and prioritize them to ensure timely completion.

6. **User Authentication:** Securely manage your projects with user accounts, ensuring that only authorized users can access and modify project information.

## Getting Started

To run the Project Management Demo App locally, follow these steps:

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/thedylanminton/project-alpha.git

2. **Navigate to the Project Directory:**
   ```bash
   python manage.py runserver

3. **Open the App in Your Browser:**

   localhost:8000
